# Deployment instructions:
Clone and enter directory:

```
git clone git@gitlab.com:perrydc/npr-flask
cd npr-flask
```

## Local machine:

```
export FLASK_APP=run.py
flask run
```

## Virtual host:
For older hosts without a 'Setup Python App' option, follow the instructions 
from [Will Haley](https://willhaley.com/blog/flask-on-bluehost/).  Otherwise:
- Open 'Setup Python App' from cPanel
- Select Python version > 3.0
- Select App directory (example: _projects/npr-flask)
- Select App Domain/URI (example: npr-flask)
- Click setup
- Scroll down to the npr-flask application under Existing applications
- Next to WGI file location enter the absolute path to your executable.  To 
find this easily, enter pwd from within the directory in the shell and copy 
that, along with run.py into the field. (example: 
/home/sourcewo/_projects/npr-flask/run.py)
- Click Save next to that field
- Next to modules, click show and add 'Flask'
- Click Add and then Update at the bottom
- Click on the App URI link to test
- (optional) To connect with existing packages, add your alternate module path 
to passenger_wsgi.py in the package folder.  Example:

  ```
  sys.path.insert(1, '/home/sourcewo/miniconda3/lib/python3.6/site-packages/')
  ```

- (optional) For error handling, add the following to your .htaccess in the 
app domain/URI folder:

  ```
  PassengerFriendlyErrorPages on
  ```

## Dedicated host:
These instructions assume you have already set up **mod_wsgi** from root.  If 
you're on a brand spankin' new server, you'll have to do this:

```
sudo apt-get install python3-pip apache2 libapache2-mod-wsgi-py3
sudo /etc/init.d/apache2 restart
```

If you're on dev-sandbox, someone has already done this for you, so you just 
need to ask your admin to:

- Initialize the script by appending to /etc/apache2/conf-available/wsgi.conf 
the location relative to the domain, followed by the location on the server:

  ```
  WSGIScriptAlias /npr-flask /var/www/html/npr-flask/run.py
  ```

- Restart the server:
  ```
  sudo /etc/init.d/apache2 restart
  ```

- (optional) To import local modules, you'll need to add a line to your run.py 
file that tells python the absolute path of the package (which you can determine 
by typing **pwd** from the command prompt in your folder.  If, for example, you 
want to grab an object called **objectName** from **fileName.py** stored in a 
folder called **data** that is in the same folder as your **run.py** file, you 
would set up those files (not forgetting to add an empty file called **__init__.py** 
to your **data** folder) and then add these lines to run.py:

```python
sys.path.insert(1, '/var/www/html/dperry/npr-flask')
from data.fileName import objectName
```

## Troubleshooting:
Browse to local page or deployed - http://YOURDOMAIN.com/npr-flask  
Errors should appear in the browser window and also go into the apache logs:

```
nano /var/log/apache2/error.log
```

if you get a module not found issue, install module from root via:

```
pip3 install PKGNAME
```

Once you trip an error, you may also get an error when you try to run it again.  To resolve, try:

```
pkill -f flask
flask run
```
